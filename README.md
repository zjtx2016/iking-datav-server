<!--
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:12:39
 * @LastEditTime : 2024-02-23 18:20:21
-->

<div align="center">
  <a href="https://datav.ikingtech.com/">
    <img src="https://datav.ikingtech.com/logo.png" alt="Logo" width="120" height="120">
  </a>

  <h3>金合可视化平台-后端服务</h3>

  <a href="http://www.ikingtech.com/">官网</a>
  |
  <a href="https://datavdoc.ikingtech.com">文档</a>
  |
  <a href="https://datav.ikingtech.com">在线演示</a>
  |
  <a href="https://gitee.com/ikingtech/iking-datav">Web前端</a>
  |
  <a href="https://gitee.com/ikingtech/iking-datav-server/issues">提交Bug</a>

  演示环境账号：iking 密码：iking.com
</div>

## 简介
金合可视化平台服务端主要为前端提供数据存储、数据库访问、文件服务、请求代理、构建独立部署包等能力。
相关文档正在奋力编写中

## 技术栈
* 💪 Midwayjs
* ⚡ MySQL
* 🍍 Nodejs 16+
* 🔥 TypeScript 5.0+
* 🔥 Typeorm 0.3+
* 🔥 PM2

## 使用说明
> 开始前请确认你的node版本大于15.0.0
1. 安装依赖：**npm install**
2. 运行项目：**npm run dev** （首次执行将会自动生成数据表）
3. 初始化数据：使用根目录`数据库初始化文件`下的.sql文件初始化数据
4. 管理员账号、密码(数据库中存储的password字段为加密后的密码)：admin/Abc123++   其它用户密码为：123456

数据库等更多配置请查看
- **[Midwayjs官方文档](https://midwayjs.org/docs/intro)**
- **[Typeorm官方文档](https://typeorm.io)**
- **[Typeorm中文文档](https://www.typeorm.org/)**

### 平台截图
<img src="https://datav.ikingtech.com/图片1.png">

<img src="https://datav.ikingtech.com/图片2.png">

<img src="https://datav.ikingtech.com/图片3.png">

## 联系我们
<h3>技术交流群</h3>
<img src="https://datav.ikingtech.com/qywx.jpg" width="200">
<br />
<br />
邮箱 - nanawfl@163.com / wfl.118@qq.com
