import { BaseEntity } from '@cool-midway/core';
import { Column, Entity, Index } from 'typeorm';

/**
 * 项目文件模块
 */
@Entity('screen_project_file')
export class FileEntity extends BaseEntity {
  @Index()
  @Column({ comment: '用户ID', length: 11, nullable: true })
  userId: string;

  @Column({ comment: '用户名称', length: 11, nullable: true })
  userName: string;

  @Column({ comment: '文件名称', length: 255 })
  fileName: string;

  @Column({ comment: '文件混淆名称', length: 255 })
  mixFileName: string;

  @Column({ comment: '文件路径', length: 255 })
  path: string;

  @Column({ comment: '文件全路径', length: 255 })
  fullPath: string;

  @Column({ comment: '文件大小', length: 255, nullable: true })
  size: string;

  @Index()
  @Column({ comment: '项目ID', length: 200 })
  projectId: string;
}
