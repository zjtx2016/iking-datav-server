/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-11-01 15:11:42
 * @LastEditTime : 2024-01-08 11:38:52
 */
import { FileEntity } from './../entity/file';
import { Provide } from '@midwayjs/decorator';
import { BaseService } from '@cool-midway/core';
import { InjectEntityModel } from '@midwayjs/typeorm';
import { In, Like, Repository } from 'typeorm';
import * as fs from 'fs';
import { join } from 'path';

@Provide()
export class FileService extends BaseService {
  @InjectEntityModel(FileEntity)
  fileEntity: Repository<FileEntity>;

  async saveProjectFile(param: FileEntity) {
    this.fileEntity.save(param);
  }

  async deleteFiles(ids) {
    const files = await this.fileEntity.find({
      where: {
        id: In(ids),
      },
    });
    for (let f of files) {
      const filePath = join(__dirname, '../../../../', f.path);
      fs.unlink(filePath, error => {
        if (error) console.info('删除项目文件异常: ', error);
      });
    }
    return this.fileEntity.delete(ids);
  }
}
