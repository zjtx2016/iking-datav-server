/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-11-01 15:04:13
 * @LastEditTime : 2024-01-08 12:19:12
 */
import { Get, Provide, Post, Body } from '@midwayjs/decorator';
import { CoolController, BaseController } from '@cool-midway/core';
import { FileEntity } from '../../entity/file';
import { FileService } from '../../service/file';
/**
 * 项目文件
 */
@Provide()
@CoolController({
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: FileEntity,
  service: FileService,
  listQueryOp: {
    keyWordLikeFields: ['projectId'],
  },
})
export class AdminScreenProjectFilesController extends BaseController {
  @Post('/deletes')
  async deleteFiles(@Body('ids') ids) {
    const res = await (this.service as FileService).deleteFiles(ids);
    return this.ok(res);
  }
}
