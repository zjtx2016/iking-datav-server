/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:22:32
 * @LastEditTime : 2024-01-12 11:42:45
 */
import { Get, Provide, Param, Post, Query, Body } from '@midwayjs/decorator';
import { CoolController, BaseController } from '@cool-midway/core';
import { Inject } from '@midwayjs/core';
import { ComponentEntity } from '../../entity/component';
import { ComponentService } from '../../service/component';
import { CollectionService } from '../../service/collection';
/**
 * 组件-组件
 */
@Provide()
@CoolController({
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: ComponentEntity,
  service: ComponentService,
  before: ctx => {
    if (
      [
        '/app/coms/component/add',
        '/app/coms/component/update',
        '/app/coms/component/collection',
      ].includes(ctx.request.url)
    ) {
      const {
        actions,
        apiData,
        apis,
        attr,
        config,
        events,
        children,
        scaling,
        animate,
        bgImg,
        dialog,
        disActions,
      } = ctx.request.body;
      ctx.request.body = {
        ...ctx.request.body,
        children: JSON.stringify(children),
        apiData: JSON.stringify(apiData),
        animate: JSON.stringify(animate || {}),
        dialog: JSON.stringify(dialog || {}),
        apis: JSON.stringify(apis),
        attr: JSON.stringify(attr),
        config: JSON.stringify(config),
        events: JSON.stringify(events),
        scaling: JSON.stringify(scaling),
        bgImg: JSON.stringify(bgImg),
        disActions: JSON.stringify(disActions),
        actions: JSON.stringify(actions || {}),
      };
    }
  },
})
export class ComponentsController extends BaseController {
  @Inject()
  componentService: ComponentService;
  @Inject()
  collectionService: CollectionService;

  @Get('/coms')
  async queryComs(@Query('screenId') id: string) {
    const res = await this.componentService.queryComsByProjectId(id as any);
    return this.ok(res);
  }

  @Post('/adds')
  async addComs(@Body() coms: ComponentEntity[]) {
    const res = await this.componentService.addComponents(coms);
    return this.ok(res);
  }

  @Post('/del')
  async deleteComs(@Body() ids: string) {
    const res = await this.componentService.deleteComponents(ids);
    return this.ok(res);
  }

  @Post('/updates')
  async updateComs(@Body() coms: ComponentEntity[]) {
    const res = await this.componentService.updateComponents(coms);
    return this.ok(res);
  }

  @Post('/copy')
  async copyComs(@Body() coms: ComponentEntity[]) {
    const res = await this.componentService.copyComponents(coms);
    return this.ok(res);
  }

  @Post('/delete/by/screen')
  async deleteComByScreen(@Body('id') id: string) {
    const res = await this.componentService.deleteComponentByScreen(id);
    return this.ok(res);
  }
}
