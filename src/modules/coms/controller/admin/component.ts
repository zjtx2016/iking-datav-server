/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:22:32
 * @LastEditTime : 2024-01-29 17:27:24
 */
import { Get, Provide, Param, Post, Query, Body } from '@midwayjs/decorator';
import { CoolController, BaseController } from '@cool-midway/core';
import { Inject } from '@midwayjs/core';
import { CollectionEntity } from '../../entity/collection';
import { CollectionService } from '../../service/collection';
import { buildShortUUID } from '../../../../comm/uuid';
/**
 * 组件-组件
 */
@Provide()
@CoolController({
  entity: CollectionEntity,
  service: CollectionService,
  before: (ctx: {
    request: {
      body: {
        isPublic?: any;
        projectId?: any;
        id?: any;
        apiData?: any;
        apis?: any;
        attr?: any;
        config?: any;
        events?: any;
        children?: any;
        scaling?: any;
        animate?: any;
        bgImg?: any;
        dialog?: any;
        disActions?: any;
        [key: string]: any;
      };
      url: string;
    };
    admin: { username: string; depId: any };
  }) => {
    ctx.request.body.isPublic = ctx.admin?.username === 'admin' ? true : false;
    ctx.request.body.projectId = ctx.admin?.depId;
    if (
      [
        '/admin/coms/component/collection',
        '/admin/coms/component/update/collection',
      ].includes(ctx.request.url)
    ) {
      const {
        apiData,
        apis,
        attr,
        config,
        events,
        children,
        scaling,
        animate,
        bgImg,
        dialog,
        disActions,
        actions,
      } = ctx.request.body;
      if (ctx.request.url === '/admin/coms/component/collection') {
        ctx.request.body.id = `${
          ctx.request.body.id.split('_')[0]
        }_${buildShortUUID()}`;
      }
      ctx.request.body = {
        ...(ctx.request.body as any),
        children: JSON.stringify(children),
        apiData: JSON.stringify(apiData),
        animate: JSON.stringify(animate || {}),
        dialog: JSON.stringify(dialog || {}),
        apis: JSON.stringify(apis),
        attr: JSON.stringify(attr),
        config: JSON.stringify(config),
        events: JSON.stringify(events),
        scaling: JSON.stringify(scaling),
        bgImg: JSON.stringify(bgImg),
        disActions: JSON.stringify(disActions),
        actions: JSON.stringify(actions || {}),
      } as any;
    }
  },
} as any)
export class ComponentsController extends BaseController {
  @Inject()
  collectionService: CollectionService;

  @Post('/collection')
  async collectionComponent(@Body() coms) {
    const res = await this.collectionService.collComponents(coms);
    return this.ok(res);
  }

  @Get('/list')
  async collectionList(@Body() body) {
    const res = await this.collectionService.collectionList(body);
    return this.ok(res);
  }

  @Post('/cancel/collection')
  async cancelCollection(@Body('id') id: string | Array<string>) {
    const res = await this.collectionService.cancelCollComponents(id);
    return this.ok(res);
  }
  @Post('/update/collection')
  async updateCollection(@Body() coms) {
    const res = await this.collectionService.updateCollComponents(coms);
    return this.ok(res);
  }
  @Post('/delete/collection')
  async deleteCollection(@Body('ids') ids) {
    const res = await this.collectionService.deleteCollComponents(ids);
    return this.ok(res);
  }
}
