/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:22:32
 * @LastEditTime : 2024-02-18 16:07:58
 */
import { BaseEntity } from '@cool-midway/core';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';

@Entity('screen_component')
export class ComponentEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ comment: '项目ID', length: 36, nullable: true })
  screenId: string;

  @Column({ comment: '排序序号', nullable: true })
  sortOrder: number;

  @Column({ comment: '类型', length: 50 })
  type: string;

  @Column({ comment: '别名', length: 50 })
  alias: string;

  @Column({ comment: '名称', length: 50 })
  name: string;

  @Column({ comment: '子组件', type: 'longtext', nullable: true })
  children: string;

  @Column({ comment: '是否锁定', default: false, nullable: true })
  locked: boolean;

  @Column({ comment: '父级ID', length: 36, nullable: true })
  parentId: string;

  @Column({ comment: '是否隐藏', default: false, nullable: true })
  hided: boolean;

  @Column({ comment: '图标', length: 255, nullable: true })
  icon: string;

  @Column({ comment: '图片', length: 255, nullable: true })
  img: string;

  @Column({ comment: '属性', length: 255, nullable: true })
  attr: string;

  @Column({ comment: '配置', type: 'text', nullable: true })
  config: string;

  @Column({ comment: 'API配置', type: 'text', nullable: true })
  apis: string;

  @Column({ comment: 'API数据', type: 'text', nullable: true })
  apiData: any;

  @Column({ comment: '入场动画', type: 'text', nullable: true })
  animate: any;

  @Column({ comment: '背景图', type: 'text', nullable: true })
  bgImg: any;

  @Column({ comment: '组内配置', type: 'text', nullable: true })
  scaling: any;

  @Column({ comment: '事件', type: 'text', nullable: true })
  events: string;

  @Column({ comment: '是否选中', default: false, nullable: true })
  selected: boolean;

  @Column({ comment: '是否悬停', default: false, nullable: true })
  hovered: boolean;

  @Column({ comment: '背景动画', type: 'text', nullable: true })
  bgAnimation: any;

  @Column({ comment: '弹窗配置', type: 'longtext', nullable: true })
  dialog: string;

  @Column({ comment: '是否为弹窗', default: false, nullable: true })
  isDialog: boolean;

  @Column({ comment: '系统响应事件', type: 'text', nullable: true })
  disActions: any;

  @Column({ comment: '自定义响应事件', type: 'text', nullable: true })
  actions: any;

  @CreateDateColumn()
  createTime: Date;

  @UpdateDateColumn()
  updateTime: Date;

  @VersionColumn()
  version: string;
}
