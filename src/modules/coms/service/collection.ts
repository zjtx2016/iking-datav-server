/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:22:32
 * @LastEditTime : 2024-02-18 16:08:58
 */
import { CollectionEntity } from './../entity/collection';
import { Provide } from '@midwayjs/decorator';
import { BaseService } from '@cool-midway/core';
import { InjectEntityModel } from '@midwayjs/typeorm';
import { Repository, In } from 'typeorm';
import { buildUUID } from '../../../comm/uuid';
@Provide()
export class CollectionService extends BaseService {
  @InjectEntityModel(CollectionEntity)
  collectionEntity: Repository<CollectionEntity>;

  async collComponents(coms: CollectionEntity) {
    const res = await this.collectionEntity.save(coms);
    return res;
  }

  async collectionList(body) {
    const res = await this.collectionEntity.query(
      `select * from screen_component_collection where isPublic = true or projectId = '${body?.projectId}' `
    );
    if (!res) return [];
    return res.map(v => {
      return {
        ...v,
        apiData: JSON.parse(v.apiData),
        apis: JSON.parse(v.apis),
        attr: JSON.parse(v.attr),
        config: JSON.parse(v.config),
        events: JSON.parse(v.events),
        children: JSON.parse(v.children),
        scaling: JSON.parse(v.scaling),
        animate: JSON.parse(v.animate),
        bgImg: JSON.parse(v.bgImg || '{}'),
        dialog: JSON.parse(v.dialog || '{}'),
        actions: JSON.parse(v.actions || '{}'),
        disActions: JSON.parse(v.disActions || []),
      };
    });
  }

  async cancelCollComponents(id: string | Array<string>) {
    const res = await this.collectionEntity.delete(
      Array.isArray(id) ? id : [id]
    );
    return res;
  }

  async updateCollComponents(coms: CollectionEntity) {
    const res = await this.collectionEntity.update({ id: coms.id }, coms);
    return res;
  }

  async deleteCollComponents(ids) {
    const idArr = Array.isArray(ids) ? ids : [ids];
    return await this.collectionEntity.delete(idArr);
  }
}
