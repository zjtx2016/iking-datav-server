/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-11 15:42:03
 * @LastEditTime : 2024-01-08 18:15:37
 */
import { BaseEntity } from '@cool-midway/core';
import { Column, Entity, Index } from 'typeorm';

@Entity('screen_info')
export class ScreenEntity extends BaseEntity {
  @Index()
  @Column({ comment: '大屏ID', length: 36, nullable: true })
  screenId: string;

  @Column({ comment: '名称', length: 50, default: '' })
  name: string;

  @Column({ comment: '组ID', length: 36, default: '' })
  groupId: string;

  @Column({ comment: '模板ID', length: 36, nullable: true })
  templateId: string;

  @Column({ comment: '分享地址', length: 200, nullable: true })
  shareUrl: string;

  @Column({ comment: '分享密码', length: 50, nullable: true })
  sharePassword: string;

  @Column({ comment: '分享过期时间', nullable: true })
  shareTime: Date;

  @Column({ comment: '是否已发布', nullable: true, default: false })
  shared: boolean;

  @Column({ comment: '宽度', default: 1920 })
  width: number;

  @Column({ comment: '高度', default: 1080 })
  height: number;

  @Column({ comment: '背景图', nullable: true })
  bgimage: string;

  @Column({ comment: '背景色', nullable: true })
  bgcolor: string;

  @Column({ comment: '间隔像素', nullable: true, default: 8 })
  grid: number;

  @Column({ comment: '缩略图', nullable: true })
  screenshot: string;

  @Column({ comment: '缩放方式', nullable: true })
  zoomMode: string;

  @Column({ comment: '使用水印', nullable: true, default: false })
  useWatermark: boolean;

  @Column({ comment: '滤镜', nullable: true })
  styleFilterParams: string;

  @Column({ comment: '子屏', nullable: true, type: 'longtext' })
  pages: any;

  @Column({ comment: '弹窗', nullable: true, type: 'longtext' })
  dialogs: any;

  @Column({ comment: '请求配置', nullable: true, type: 'longtext' })
  address: any;

  @Column({ comment: '变量', nullable: true, type: 'longtext' })
  variables: any;

  @Column({ comment: '模板图片地址', nullable: true, type: 'varchar' })
  thumbnail: any;

  @Column({ comment: 'HOST配置', nullable: true, type: 'longtext' })
  host: any;

  @Column({ comment: '全局事件', nullable: true, type: 'longtext' })
  events: any;

  @Column({ comment: '默认子屏', nullable: true, type: 'varchar' })
  defaultPage: string;

  @Column({
    comment: '嵌入配置',
    type: 'longtext',
    nullable: true,
  })
  iframe: string;
}
