/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-11 15:42:03
 * @LastEditTime : 2024-02-23 18:25:41
 */
import { Get, Provide, Param, Post, Query, Body } from '@midwayjs/decorator';
import { Inject } from '@midwayjs/core';
import { CoolController, BaseController } from '@cool-midway/core';
import { ScreenEntity } from '../../entity/screen';
import { ScreenService } from '../../service/screen';
import { BaseSysDepartmentService } from '../../../base/service/sys/department';
import { ComponentService } from '../../../coms/service/component';
import { FilterService } from '../../../filter/service/filter';
import { ScreenReleaseService } from '../../../release/service/release';
// import { ZipService } from '../../service/zip';
import * as archiver from 'archiver';

import * as fs from 'fs';
import { join } from 'path';
import * as fse from 'fs-extra';

const JSON_PATH = '../../../../../nginx/server/source/';
const STATIC_PATH = '../../../../../nginx/public/uploads/';

/**
 * 项目-大屏
 */
@Provide()
@CoolController({
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: ScreenEntity,
  service: ScreenService,
  listQueryOp: {
    keyWordLikeFields: ['name'],
  },
  before: ctx => {
    if (
      ['/app/screen/screen/add', '/app/screen/screen/update'].includes(
        ctx.request.url
      )
    ) {
      const {
        pages,
        dialogs,
        variables,
        styleFilterParams,
        host,
        events,
        iframe,
      } = ctx.request.body;
      ctx.request.body = {
        ...ctx.request.body,
        pages: JSON.stringify(pages),
        dialogs: JSON.stringify(dialogs),
        variables: JSON.stringify(variables),
        host: JSON.stringify(host),
        events: JSON.stringify(events),
        iframe: JSON.stringify(iframe),
        styleFilterParams: JSON.stringify(styleFilterParams),
      };
    }
  },
  pageQueryOp: {
    keyWordLikeFields: ['name'],
    // fieldEq: ['groupId'],
    addOrderBy: {
      createTime: 'desc',
    },
    where: ctx => {
      const { groupId } = ctx.request.body;
      return [['groupId LIKE :groupId', { groupId: `${groupId}%` }]];
    },
  },
})
export class ScreensController extends BaseController {
  @Inject()
  baseSysDepartmentService: BaseSysDepartmentService;

  @Inject()
  componentService: ComponentService;

  @Inject()
  filterService: FilterService;

  @Inject()
  screenReleaseService: ScreenReleaseService;

  // @Inject()
  // zipService: ZipService;

  @Get('/:id')
  async queryScreenById(@Param('id') id: string) {
    const res = await (this.service as any).queryById(id);
    return this.ok(res);
  }
  /**
   * @description: 根据projectId查询所有大屏
   * @return {*}
   */
  @Get('/apps')
  async queryScreenInProject(@Query() param: any) {
    const res = await (this.service as any).getScreenWithChildren(param);
    return this.ok(res);
  }

  @Post('/copy')
  async copyScreen(@Body('id') id: string) {
    const res = await (this.service as any).copyScreen(id);
    return this.ok(res);
  }

  @Post('/rename')
  async renameScreen(@Body() obj: { id: string; name: string }) {
    const res = await (this.service as any).renameScreen(obj.id, obj.name);
    return this.ok(res);
  }

  @Post('/publish/info')
  async publishInfo(@Body('id') id: string) {
    const res = await (this.service as any).publishInfo(id);
    return this.ok(res);
  }

  @Post('/publish')
  async publishScreen(@Body() screen: Object) {
    const res = await (this.service as any).publishScreen(screen);
    return this.ok(res);
  }

  @Post('/template')
  async templateScreen(@Body() data: any) {
    const res = await (this.service as any).templateScreen(data);
    return res ? this.ok(res) : this.fail();
  }
}
