/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-09-25 18:15:04
 * @LastEditTime : 2023-10-18 10:34:29
 */
import { BaseEntity } from '@cool-midway/core';
import { Column, Index, Entity } from 'typeorm';

/**
 * 文件空间信息
 */
@Entity('space_info')
export class SpaceInfoEntity extends BaseEntity {
  @Column({ comment: '地址' })
  url: string;

  @Column({ comment: '类型' })
  type: string;

  @Column({ comment: '分类ID', type: 'bigint', nullable: true })
  classifyId: number;

  @Column({ comment: '大屏ID', type: 'varchar', nullable: true })
  screenId: number;

  @Column({ comment: '项目ID', type: 'varchar', nullable: true })
  projectId: number;

  @Index()
  @Column({ comment: '文件id' })
  fileId: string;

  @Column({ comment: '文件名' })
  name: string;

  @Column({ comment: '文件大小' })
  size: number;

  @Column({ comment: '文档版本', default: 1 })
  version: number;

  @Column({ comment: '文件位置' })
  key: string;
}
