/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:22:32
 * @LastEditTime : 2023-12-29 17:04:16
 */
import { Config, Get, Provide, Post, Body } from '@midwayjs/decorator';
import { CoolController, BaseController } from '@cool-midway/core';
import { SpaceInfoEntity } from '../../entity/info';
import { SpaceInfoService } from '../../service/info';

/**
 * 图片空间信息
 */
@Provide()
@CoolController({
  api: ['add', 'update', 'info', 'list', 'page'],
  entity: SpaceInfoEntity,
  service: SpaceInfoService,
  pageQueryOp: {
    fieldEq: ['type', 'classifyId'],
  },
})
export class BaseAppSpaceInfoController extends BaseController {
  @Config('module.space.wps')
  config;

  @Get('/getConfig', { summary: '获得WPS配置' })
  async getWPSConfig() {
    return this.ok({ appId: this.config.appId });
  }

  @Post('/delete', { summary: '删除图片' })
  async getConfig(@Body('ids') ids) {
    const res = await (this.service as any).deleteCustom(ids);
    return this.ok(res);
  }
}
