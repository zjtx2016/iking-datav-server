import { BaseEntity } from '@cool-midway/core';
import { Column, Entity, Index } from 'typeorm';

/**
 * Font Family Management
 */
@Entity('screen_font_family')
export class FontFamilyEntity extends BaseEntity {
  @Index()
  @Column({ comment: 'ID', primary: true, generated: true })
  id: number;

  @Column({ comment: 'URL', length: 255 })
  url: string;

  @Column({ comment: 'Project ID', length: 255 })
  projectId: string;

  @Column({ comment: 'Is System', default: true })
  isSys: boolean;

  @Column({ comment: 'Name', length: 255 })
  name: string;
}
