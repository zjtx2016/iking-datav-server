import { Get, Provide } from '@midwayjs/decorator';
import { CoolController, BaseController } from '@cool-midway/core';
import { FontFamilyEntity } from '../../entity/family';

/**
 * 字体管理
 */
@Provide()
@CoolController({
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: FontFamilyEntity,
})
export class AdminFontFamiliesController extends BaseController {}
