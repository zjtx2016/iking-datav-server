/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-11-07 19:02:41
 * @LastEditTime : 2023-11-08 09:16:14
 */
import { BaseEntity } from '@cool-midway/core';
import { Column, Entity, Index } from 'typeorm';

/**
 * datasource模块-screen_datasource
 */
@Entity('screen_data_source')
export class ScreenDatasourceEntity extends BaseEntity {
  @Index()
  @Column({ comment: '名称', length: 50 })
  name: string;
  @Column({ comment: '类型', length: 50 })
  type: string;
}
