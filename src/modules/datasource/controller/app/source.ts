/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-11-07 19:02:41
 * @LastEditTime : 2023-11-07 19:29:14
 */
import { Get, Provide } from '@midwayjs/decorator';
import { CoolController, BaseController } from '@cool-midway/core';
import { ScreenDatasourceEntity } from '../../entity/source';

/**
 * 数据源-屏幕
 */
@Provide()
@CoolController({
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: ScreenDatasourceEntity,
  listQueryOp: {
    addOrderBy: {
      id: 'asc',
    },
  },
})
export class ScreenDatasourcesController extends BaseController {}
