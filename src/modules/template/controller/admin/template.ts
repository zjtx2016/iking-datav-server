/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-12-27 09:26:35
 * @LastEditTime : 2024-01-17 15:17:02
 */
import { Get, Provide, Post, Body } from '@midwayjs/decorator';
import { CoolController, BaseController } from '@cool-midway/core';
import { TemplateEntity } from '../../entity/template';
import { TemplateService } from '../../service/template';
/**
 * 模板-屏幕
 */
@Provide()
@CoolController({
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: TemplateEntity,
  service: TemplateService,
  pageQueryOp: {
    keyWordLikeFields: ['name'],
    fieldEq: ['isSystem'],
  },
  // before: async ctx => {
  //   if (['/app/template/template/page'].includes(ctx.request.url)) {
  //     ctx.request.body = {
  //       ...ctx.request.body,
  //       system: ctx.request.body?.system ? 1 : 0,
  //     };
  //   }
  // },
})
export class TemplatesController extends BaseController {
  @Post('/template/add')
  async addTemplate(@Body() data: any) {
    const success = await (this.service as any).addTemplate({
      ...data,
      userId: this.baseCtx.admin.id,
      isSystem: this.baseCtx.admin.username === 'admin'
    });
    return success ? this.ok() : this.fail();
  }
}
