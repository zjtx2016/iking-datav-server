/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-12 16:01:36
 * @LastEditTime : 2023-12-29 19:06:54
 */
import { BaseEntity } from '@cool-midway/core';
import { Column, Entity, Index, VersionColumn } from 'typeorm';

@Entity('screen_template_filter')
export class TemplateFilterEntity extends BaseEntity {
  @Column({ comment: '过滤器ID', length: 50, unique: true, nullable: true })
  filterId: string;

  @Column({ comment: '名称', length: 50, unique: true })
  name: string;

  @Column({ comment: 'Code', type: 'longtext' })
  code: string;

  @Column({ comment: '来源', type: 'longtext' })
  origin: string;

  @Column({ comment: '模板ID', length: 50 })
  templateId: string;

  @Column({ comment: '项目ID', length: 50 })
  projectId: string;

  @Column({ comment: '屏幕ID', length: 50 })
  screenId: string;

  @VersionColumn({ comment: '版本' })
  version: string;
}
