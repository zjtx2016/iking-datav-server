import { BaseEntity } from '@cool-midway/core';
import { Column, Entity, Index } from 'typeorm';

@Entity('screen_template')
export class TemplateEntity extends BaseEntity {
  @Index()
  @Column({ comment: 'ID', primary: true })
  id: number;

  @Column({ comment: '用户ID', nullable: true })
  userId: string;

  @Column({ comment: '描述', type: 'boolean', default: false })
  isSystem: boolean;

  @Column({ comment: '描述', nullable: true })
  description: string;

  @Column({ comment: '配置', type: 'longtext' })
  config: string;

  @Column({ comment: '名称' })
  name: string;

  @Column({ comment: '宽度' })
  width: number;

  @Column({ comment: '高度' })
  height: number;

  @Column({ comment: '快照', nullable: true })
  snapshot: string;

  @Column({ comment: '组件', type: 'text' })
  coms: string;
}
