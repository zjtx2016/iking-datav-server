/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-11-07 19:28:12
 * @LastEditTime : 2023-12-29 16:05:03
 */
import { BaseEntity } from '@cool-midway/core';
import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';
/**
 * database模块-screen_data_base
 */
@Entity('screen_data_base')
export class ScreenDataBaseEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: any;

  @Index()
  @Column({ comment: '名称', length: 50 })
  name: string;

  @Index()
  @Column({ comment: '数据源', length: 50 })
  type: string;

  @Column({ comment: '数据库地址', length: 100 })
  host: string;

  @Column({ comment: '端口', type: 'smallint' })
  port: number;

  @Column({ comment: '用户名', length: 50 })
  username: string;

  @Column({ comment: '密码', length: 50 })
  password: string;

  @Column({ comment: '用户ID', length: 50 })
  userId: string;

  @Column({ comment: '项目ID', length: 50 })
  depId: string;
}
