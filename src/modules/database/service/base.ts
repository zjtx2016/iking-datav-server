/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-11-07 19:31:05
 * @LastEditTime : 2024-01-05 12:00:46
 */
import { ScreenDataBaseEntity } from '../entity/base';
import { Provide, Inject } from '@midwayjs/decorator';
import { BaseService } from '@cool-midway/core';
import { InjectEntityModel } from '@midwayjs/typeorm';
import { Repository } from 'typeorm';

@Provide()
export class ScreenDataBaseService extends BaseService {
  @InjectEntityModel(ScreenDataBaseEntity)
  ScreenDataBaseEntity: Repository<ScreenDataBaseEntity>;

  @Inject()
  ctx;

  async queryBasesId(id: string) {
    return await this.ScreenDataBaseEntity.findOneBy({ id });
  }

  async databaseList(body) {
    return body?.depId
      ? await this.ScreenDataBaseEntity.find({
          where: {
            depId: body.depId,
          },
        })
      : await this.ScreenDataBaseEntity.find();
  }
}
