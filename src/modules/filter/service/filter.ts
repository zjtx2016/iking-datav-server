/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:22:32
 * @LastEditTime : 2024-01-29 17:31:17
 */
import { FilterEntity } from './../entity/filter';
import { Provide } from '@midwayjs/decorator';
import { InjectEntityModel } from '@midwayjs/typeorm';
import { BaseService } from '@cool-midway/core';
import { In, Like, Repository } from 'typeorm';

@Provide()
export class FilterService extends BaseService {
  @InjectEntityModel(FilterEntity)
  filterEntity: Repository<FilterEntity>;

  async getFilterList(body = {} as any) {
    const { projectId, screenId } = body;
    return screenId
      ? await this.filterEntity.find({
          where: {
            projectId: Like(`${body?.projectId}%`),
            screenId: body?.screenId || '',
          },
        })
      : await this.filterEntity.find({
          where: {
            projectId: Like(`${body?.projectId}%`),
          },
        });
  }
}
