/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:22:32
 * @LastEditTime : 2024-01-29 17:30:49
 */
import { Get, Provide, Post, Body } from '@midwayjs/decorator';
import { CoolController, BaseController } from '@cool-midway/core';
import { FilterEntity } from '../../entity/filter';
import { FilterService } from '../../service/filter';
/**
 * 过滤器
 */
@Provide()
@CoolController({
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: FilterEntity,
  service: FilterService,
  before: ctx => {
    if (
      ['/admin/filter/filter/add', '/admin/filter/filter/update'].includes(
        ctx.request.url
      )
    ) {
      const { id, filterId, name, code, origin, projectId, screenId, version } =
        ctx.request.body;
      const obj: any = {
        filterId,
        name,
        code,
        origin,
        projectId,
        screenId,
        version,
      };
      if (id) {
        obj.id = id;
      }
      ctx.request.body = obj;
    } else if (
      ['/admin/filter/filter/lists', '/admin/filter/filter/page'].includes(
        ctx.request.url
      )
    ) {
      ctx.request.body = {
        projectId: ctx?.admin?.depId,
        ...ctx.request.body,
      };
    }
  },
})
export class FiltersController extends BaseController {
  @Post('/lists')
  async getFilterList(@Body() body) {
    const res = await (this.service as FilterService).getFilterList(body);
    return res ? this.ok(res) : this.fail();
  }
}
