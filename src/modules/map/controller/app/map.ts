import { Get, Provide, Body, Post } from '@midwayjs/decorator';
import { CoolController, BaseController } from '@cool-midway/core';
import { MapScreenMapEntity } from '../../entity/map';
import { MapScreenService } from '../../service/map';

/**
 * 地图-地图
 */
@Provide()
@CoolController({
  api: ['add', 'update', 'info', 'list', 'page'],
  entity: MapScreenMapEntity,
  service: MapScreenService,
})
export class AdminMapMapsController extends BaseController {
  @Post('/delete', { summary: '删除地图' })
  async getConfig(@Body('id') id) {
    const res = await (this.service as any).deleteById(id);
    return this.ok(res);
  }
}
