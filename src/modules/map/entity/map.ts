import { BaseEntity } from '@cool-midway/core';
import { Column, Entity, Index } from 'typeorm';

/**
 * map模块-screen_map
 */
@Entity('screen_map')
export class MapScreenMapEntity extends BaseEntity {
  @Index()
  @Column({ comment: 'ID', primary: true })
  id: number;

  @Column({ comment: '用户ID', length: 11, nullable: true })
  userId: string;

  @Column({ comment: 'uid', length: 50, nullable: true })
  uid: string;

  @Column({ comment: 'JSON', type: 'longtext', nullable: true })
  jsonData: any;

  @Column({ comment: '名称', length: 50 })
  name: string;

  @Column({ comment: '文件', length: 255, nullable: true })
  file: string;
}
