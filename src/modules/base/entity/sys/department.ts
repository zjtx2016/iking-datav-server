import { BaseEntity } from '@cool-midway/core';
import { Column, Entity } from 'typeorm';

/**
 * 部门
 */
@Entity('base_sys_department')
export class BaseSysDepartmentEntity extends BaseEntity {
  @Column({ comment: '部门名称' })
  name: string;

  @Column({ comment: '部门ID', type: 'varchar', nullable: true })
  depId: string;

  @Column({
    comment: '是否删除',
    type: 'boolean',
    nullable: true,
    default: false,
  })
  del: boolean;

  @Column({ comment: '上级部门ID', type: 'varchar', nullable: true })
  parentId: string;

  @Column({ comment: '排序', default: 0 })
  orderNum: number;
  // 父菜单名称
  parentName: string;
}
