/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:22:32
 * @LastEditTime : 2024-01-15 15:22:43
 */
import { ScreenReleaseEntity } from './../entity/release';
import { Provide } from '@midwayjs/decorator';
import { BaseService } from '@cool-midway/core';
import { InjectEntityModel } from '@midwayjs/typeorm';
import { Repository, In } from 'typeorm';
@Provide()
export class ScreenReleaseService extends BaseService {
  @InjectEntityModel(ScreenReleaseEntity)
  screenReleaseEntity: Repository<ScreenReleaseEntity>;

  async addRelease(param: ScreenReleaseEntity) {
    const res = await this.screenReleaseEntity.save(param);
    return res;
  }
}
