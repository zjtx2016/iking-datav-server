/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-01-15 14:57:57
 * @LastEditTime : 2024-01-17 09:18:27
 */
import { BaseEntity } from '@cool-midway/core';
import {
  Column,
  Entity,
  Index,
  CreateDateColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';

/**
 * release模块-screen_release
 */
@Entity('screen_release')
export class ScreenReleaseEntity extends BaseEntity {
  @Index()
  @Column({ comment: 'ID', primary: true })
  id: number;

  @Column({ comment: '名称' })
  name: string;

  @Column({ comment: '项目ID' })
  projectId: string;

  @Column({ comment: '应用ID' })
  screenId: string;

  @Column({ comment: '用户名', nullable: true })
  userName: string;

  @Column({ comment: '文件URL', length: 255 })
  fileUrl: string;

  @Column({ comment: '超时时间', nullable: true })
  timeOut: Date;

  @CreateDateColumn()
  createTime: Date;

  @UpdateDateColumn()
  updateTime: Date;

  @VersionColumn()
  version: string;
}
