/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-01-15 14:57:57
 * @LastEditTime : 2024-01-16 19:59:51
 */
import { Get, Provide } from '@midwayjs/decorator';
import { CoolController, BaseController } from '@cool-midway/core';
import { ScreenReleaseEntity } from '../../entity/release';

/**
 * 发布-屏幕
 */
@Provide()
@CoolController({
  api: ['add', 'delete', 'update', 'info', 'list', 'page'],
  entity: ScreenReleaseEntity,
  pageQueryOp: {
    fieldEq: ['projectId'],
  },
  before: ctx => {
    ctx.request.body.userName = ctx.admin?.username;
  },
})
export class ScreenReleasesController extends BaseController {}
