/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-12-15 09:48:00
 * @LastEditTime : 2023-12-29 13:41:10
 */
import { makeHttpRequest } from '@midwayjs/core';

async function getAccessToken() {
  const res = await makeHttpRequest(
    'https://aip.baidubce.com/oauth/2.0/token?client_id=3y3iSY7hg15fhiw5vKPpDkqs&client_secret=M6nTYGEAbnv06NrWzgZK3eOwGO1099n0&grant_type=client_credentials',
    {
      method: 'POST',
      dataType: 'json',
      contentType: 'json',
    }
  );
  return (res.data as any)?.access_token;
}

export const useWxyy = async (message: any[]) => {
  const token = await getAccessToken();
  const res = await makeHttpRequest(
    `https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions_pro?access_token=${token}`,
    {
      data: {
        messages: message,
        // stream: true,
      },
      method: 'POST',
      dataType: 'json',
      contentType: 'json',
      timeout: 500000,
    }
  );
  return res.data;
};
