/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-04 17:47:55
 * @LastEditTime : 2024-02-04 18:33:36
 */
import { Provide, Inject } from '@midwayjs/decorator';
import { SocketController } from './socket.controller';

@Provide()
export class SocketManagerService {
  @Inject()
  socketController: SocketController;

  sendMessageToAll(msg: string) {
    this.socketController.sendMessageToAll(msg);
  }
}
