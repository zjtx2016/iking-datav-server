/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-02-04 15:47:12
 * @LastEditTime : 2024-02-04 18:02:52
 */
import {
  WSController,
  OnWSConnection,
  Inject,
  OnWSMessage,
  WSEmit,
} from '@midwayjs/core';
import { Context } from '@midwayjs/socketio';

@WSController('/')
export class SocketController {
  @Inject()
  ctx: Context;

  @OnWSConnection()
  async onConnectionMethod() {
    console.log('客户端已连接: ', this.ctx.id);
    this.ctx.emit('notification', 'SUCCESS');
  }

  @OnWSMessage('client')
  async gotMessage(data) {
    console.log('客户端消息: ', this.ctx.id, data);
  }

  @OnWSMessage('/message/send')
  public sendMessageToAll(msg: string) {
    this.ctx.emit('notification', msg);
  }
}
